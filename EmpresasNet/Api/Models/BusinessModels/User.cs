﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Models.BusinessModels
{
    public class User
    {
        public int Id { get; set; }
        public string Password { get; set; }
        public string Investor_Name { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public double Balance { get; set; }
        public string Photo { get; set; }
        public double Portfolio_Value { get; set; }
        public bool First_Access { get; set; }
        public bool Super_Angel { get; set; }


        public int EnterpriseId { get; set; }
        public virtual Enterprise Enterprise { get; set; }

        public virtual List<Portfolio> Portfolio { get; set; }
    }
}