﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Models.BusinessModels
{
    public class Enterprise
    {
        public int Id { get; set; }
        public string Enterprise_name { get; set; }
        public string Description { get; set; }
        public string Email_Enterprise { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Linkedin { get; set; }
        public string Phone { get; set; }
        public bool Own_Enterprise { get; set; }
        public string Photo { get; set; }
        public int Value { get; set; }
        public int Shares { get; set; }
        public double Share_Price { get; set; }
        public int Own_Shares { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public int Enterprise_TypeId { get; set; }
        public virtual EnterpriseType Enterprise_Type { get; set; }

        public virtual List<Portfolio> Portfolios { get; set; }
    }

    public class EnterpriseType
    {
        public int Id { get; set; }
        public string Enterprise_Type_Name { get; set; }
    }
}