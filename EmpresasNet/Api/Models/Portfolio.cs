﻿using Api.Models.BusinessModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Api.Models
{
    public class Portfolio
    {
        [Key]
        [Column(Order = 1)]
        public int EnterpriseId { get; set; }

        [Key]
        [Column(Order = 2)]
        public int UserId { get; set; }

        public virtual Enterprise Enterprise { get; set; }
        public virtual User User { get; set; }
    }
}
