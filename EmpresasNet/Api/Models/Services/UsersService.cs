﻿using Api.Models.BusinessModels;
using Api.Models.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Models.Services
{
    public class UsersService
    {
        private EnterpriseContext _context { get; set; }

        public UsersService(EnterpriseContext ctx)
        {
            _context = ctx;
        }

        public User ValidarLogin(string userName, string password)
        {
            return _context.Users.Where(item => item.Email == userName && item.Password == password).FirstOrDefault();
        }
    }
}