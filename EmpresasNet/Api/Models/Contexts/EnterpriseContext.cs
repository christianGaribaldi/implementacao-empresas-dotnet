﻿using Api.Models.BusinessModels;
using MySql.Data.Entity;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Api.Models.Contexts
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class EnterpriseContext : DbContext
    {
        public virtual DbSet<Enterprise> Enterprises { get; set; }
        public virtual DbSet<User> Users { get; set; }

        public EnterpriseContext() : base("name=EnterprisesContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<EnterpriseContext, DbMigrationsConfiguration<Api.Models.Contexts.EnterpriseContext>>());
        }

        // Construtor para uso de conexões já abertas
        public EnterpriseContext(DbConnection existingConnection, bool contextOwnsConnection) : base(existingConnection, contextOwnsConnection)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}