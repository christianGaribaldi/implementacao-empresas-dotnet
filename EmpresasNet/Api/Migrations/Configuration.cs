namespace Api.Migrations
{
    using Models;
    using Models.BusinessModels;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Api.Models.Contexts.EnterpriseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Api.Models.Contexts.EnterpriseContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            Enterprise e = new Enterprise()
            {
                Enterprise_name = "AllRide",
                Description = "Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the creation of citizen green areas. With this we are creating smarter cities from the people and at the same time the forest city.  Urbanatika, Agro-Urban Industry",
                Own_Enterprise = false,
                Photo = "/uploads/enterprise/photo/1/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.jpg",
                Shares = 100,
                Share_Price = 10000,
                City = "Santiago",
                Country = "Chile",
                Enterprise_Type = new EnterpriseType() { Enterprise_Type_Name = "Software" }
            };

            context.Enterprises.AddOrUpdate(e);
            List<Portfolio> portfolios = new List<Portfolio>();
            portfolios.Add(new Portfolio() { Enterprise = e });

            context.Users.AddOrUpdate(new Models.BusinessModels.User()
            {
                Investor_Name = "Teste Apple",
                Email = "testeapple@ioasys.com.br",
                City = "BH",
                Country = "Brasil",
                Balance = 1000000,
                Portfolio = portfolios,
                Portfolio_Value = 1000000,
                First_Access = false,
                Super_Angel = false
            });
        }
    }
}
