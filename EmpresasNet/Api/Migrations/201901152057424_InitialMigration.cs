namespace Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Enterprise",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Enterprise_name = c.String(),
                        Description = c.String(),
                        Email_Enterprise = c.String(),
                        Facebook = c.String(),
                        Twitter = c.String(),
                        Linkedin = c.String(),
                        Phone = c.String(),
                        Own_Enterprise = c.Boolean(nullable: false),
                        Photo = c.String(),
                        Value = c.Int(nullable: false),
                        Shares = c.Int(nullable: false),
                        Share_Price = c.Double(nullable: false),
                        Own_Shares = c.Int(nullable: false),
                        City = c.String(),
                        Country = c.String(),
                        Enterprise_TypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EnterpriseType", t => t.Enterprise_TypeId)
                .Index(t => t.Enterprise_TypeId);
            
            CreateTable(
                "dbo.EnterpriseType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Enterprise_Type_Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Portfolio",
                c => new
                    {
                        EnterpriseId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.EnterpriseId, t.UserId })
                .ForeignKey("dbo.Enterprise", t => t.EnterpriseId)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.EnterpriseId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Password = c.String(),
                        Investor_Name = c.String(),
                        Email = c.String(),
                        City = c.String(),
                        Country = c.String(),
                        Balance = c.Double(nullable: false),
                        Photo = c.String(),
                        Portfolio_Value = c.Double(nullable: false),
                        First_Access = c.Boolean(nullable: false),
                        Super_Angel = c.Boolean(nullable: false),
                        EnterpriseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Enterprise", t => t.EnterpriseId)
                .Index(t => t.EnterpriseId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Portfolio", "UserId", "dbo.User");
            DropForeignKey("dbo.User", "EnterpriseId", "dbo.Enterprise");
            DropForeignKey("dbo.Portfolio", "EnterpriseId", "dbo.Enterprise");
            DropForeignKey("dbo.Enterprise", "Enterprise_TypeId", "dbo.EnterpriseType");
            DropIndex("dbo.User", new[] { "EnterpriseId" });
            DropIndex("dbo.Portfolio", new[] { "UserId" });
            DropIndex("dbo.Portfolio", new[] { "EnterpriseId" });
            DropIndex("dbo.Enterprise", new[] { "Enterprise_TypeId" });
            DropTable("dbo.User");
            DropTable("dbo.Portfolio");
            DropTable("dbo.EnterpriseType");
            DropTable("dbo.Enterprise");
        }
    }
}
