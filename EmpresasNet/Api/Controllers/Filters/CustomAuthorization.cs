﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Api.Controllers.Filters
{
    public class CustomAuthorization : AuthorizationFilterAttribute
    {
        private const string _accessTokenCustomHeader = "access-token";
        private const string _uidCustomHeader = "uid";
        private const string _clientCustomHeader = "client";

        public override void OnAuthorization(HttpActionContext filterContext)
        {
            try
            {
                var accessToken = filterContext.Request.Headers.SingleOrDefault(x => x.Key == _accessTokenCustomHeader);
                var uid = filterContext.Request.Headers.SingleOrDefault(x => x.Key == _uidCustomHeader);
                var client = filterContext.Request.Headers.SingleOrDefault(x => x.Key == _clientCustomHeader);

                if (accessToken.Key != null && uid.Key != null && client.Key != null)
                {
                    string tokenValue = Convert.ToString(accessToken.Value.SingleOrDefault());
                    string uidValue = Convert.ToString(uid.Value.SingleOrDefault());
                    string clientValue = Convert.ToString(client.Value.SingleOrDefault());

                    if (!IsAuthorized(tokenValue, uidValue, clientValue))
                    {
                        filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                        return;
                    }
                }
                else
                {
                    filterContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                    return;
                }
            }
            catch (Exception)
            {
                filterContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                return;
            }

            base.OnAuthorization(filterContext);
        }

        private bool IsAuthorized(string token, string uid, string client)
        {
            bool result = false;
            // validar token, uid e client.

            return result;
        }
    }
}