﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Api.Models.BusinessModels;
using Api.Models.Contexts;
using Api.Models.Services;
using Api.Controllers.Filters;

namespace Api.Controllers
{
    [Route("api/{api_version:length(2)}/Enterprises")]
    [CustomAuthorization]
    public class EnterprisesController : ApiController
    {
        private UsersService _service { get; set; }
        private EnterpriseContext _context = new EnterpriseContext();

        public EnterprisesController()
        {
            _context = new EnterpriseContext();
            _service = new UsersService(_context);
        }

        // GET: api/Enterprises
        public IQueryable<Enterprise> GetEnterprises()
        {
            return _context.Enterprises;
        }

        // GET: api/Enterprises with filter
        public IHttpActionResult GetEnterprises(int enterprise_type, string name)
        {
            IQueryable<Enterprise> enterprises= _context.Enterprises.Where(item => item.Enterprise_name == name && item.Enterprise_TypeId == enterprise_type);
            if (enterprises == null)
            {
                return NotFound();
            }

            return Ok(enterprises.ToList());
        }

        // GET: api/Enterprises/5
        [ResponseType(typeof(Enterprise))]
        public async Task<IHttpActionResult> GetEnterprise(int id)
        {
            Enterprise enterprise = await _context.Enterprises.FindAsync(id);
            if (enterprise == null)
            {
                return NotFound();
            }

            return Ok(enterprise);
        }

        // PUT: api/Enterprises/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutEnterprise(int id, Enterprise enterprise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != enterprise.Id)
            {
                return BadRequest();
            }

            _context.Entry(enterprise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EnterpriseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Enterprises
        [ResponseType(typeof(Enterprise))]
        public async Task<IHttpActionResult> PostEnterprise(Enterprise enterprise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Enterprises.Add(enterprise);
            await _context.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = enterprise.Id }, enterprise);
        }

        // DELETE: api/Enterprises/5
        [ResponseType(typeof(Enterprise))]
        public async Task<IHttpActionResult> DeleteEnterprise(int id)
        {
            Enterprise enterprise = await _context.Enterprises.FindAsync(id);
            if (enterprise == null)
            {
                return NotFound();
            }

            _context.Enterprises.Remove(enterprise);
            await _context.SaveChangesAsync();

            return Ok(enterprise);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EnterpriseExists(int id)
        {
            return _context.Enterprises.Count(e => e.Id == id) > 0;
        }
    }
}