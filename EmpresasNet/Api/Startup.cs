﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using Api.Models.Services;
using System.Security.Claims;
using Api.Models.BusinessModels;
using System.Linq;

[assembly: OwinStartup(typeof(Api.Startup))]

namespace Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            HttpConfiguration config = new HttpConfiguration();

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
               name: "DefaultApi",
               routeTemplate: "api/{controller}/{id}",
               defaults: new { id = RouteParameter.Optional }
           );

            //Configuração OAuth 2.0 Token
            ConfigurarOAuthToken(app);

            // Ativando Cors
            app.UseCors(CorsOptions.AllowAll);

            // configuração WebApi
            app.UseWebApi(config);
        }

        private void ConfigurarOAuthToken(IAppBuilder app)
        {
            var opcoesConfiguracaoToken = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(1),
                Provider = new GeradorTokens(),
            };
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
            app.UseOAuthAuthorizationServer(opcoesConfiguracaoToken);
        }
    }

    public class GeradorTokens : OAuthAuthorizationServerProvider
    {
        private bool IsAuthorized(string token, string uid, string client)
        {
            bool result = false;
            // validar token, uid e client.

            return result;
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            const string _accessTokenCustomHeader = "access-token";
            const string _uidCustomHeader = "uid";
            const string _clientCustomHeader = "client";

            var accessToken = context.Request.Headers.SingleOrDefault(x => x.Key == _accessTokenCustomHeader);
            var uid = context.Request.Headers.SingleOrDefault(x => x.Key == _uidCustomHeader);
            var client = context.Request.Headers.SingleOrDefault(x => x.Key == _clientCustomHeader);
            
            if (accessToken.Key != null && uid.Key != null && client.Key != null)
            {
                string tokenValue = Convert.ToString(accessToken.Value.SingleOrDefault());
                string uidValue = Convert.ToString(uid.Value.SingleOrDefault());
                string clientValue = Convert.ToString(client.Value.SingleOrDefault());

                if (!IsAuthorized(tokenValue, uidValue, clientValue))
                {
                    context.SetError("Não autorizado");
                    return;
                }
            }
            else
            {
                context.SetError("Não autorizado");
                return;
            }
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            UsersService service = new UsersService(new Models.Contexts.EnterpriseContext());
            User user = service.ValidarLogin(context.UserName, context.Password);

            if (user != null)
            {
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);

                context.Response.Headers.Add("uid", new string[] { user.Email });
                context.Response.Headers.Add("client", new string[] { context.ClientId });
                context.Validated(identity);
            }
            else
            {
                context.SetError("Acesso Inválido", "Credenciais inválidas.");
                return;
            }
        }

        public override Task TokenEndpointResponse(OAuthTokenEndpointResponseContext context)
        {
            string accessToken = context.AccessToken;
            context.Response.Headers.Add("access-token", new string[] { accessToken });
            return base.TokenEndpointResponse(context);
        }
    }

}
